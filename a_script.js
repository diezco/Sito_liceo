function adapt() {
    var h = screen.height;
    h = h - ((h - h % 100) / 100) * 27;
    console.log(h);
    document.getElementById("content").style.height = h + "px";
}
function checkValidity(check) {
    if (check >= 2) {
        if (check <= 6) {
            return true;
        } else {
            console.log(check + ": input number to high");
            return false;
        }
    } else {
        console.log(check + ": input number to low");
        return false;
    }
}
function mark() {
    var os = document.getElementById("OS").value;
    var matematica = document.getElementById("Matematica").value;
    var oc = document.getElementById("OC").value;
    var arti = document.getElementById("Arti").value;
    var fisica = document.getElementById("Fisica").value;
    var chimica = document.getElementById("Chimica").value;
    var biologia = document.getElementById("Biologia").value;
    var italiano = document.getElementById("Italiano").value;
    var l2 = document.getElementById("L2").value;
    var l3 = document.getElementById("L3").value;
    var filosofia = document.getElementById("Filosofia").value;
    var geografia = document.getElementById("Geografia").value;
    var storia = document.getElementById("Storia").value;
    var os2 = document.getElementById("OS2").value;
    var matematica2 = document.getElementById("Matematica2").value;
    var italiano2 = document.getElementById("Italiano2").value;
    var l22 = document.getElementById("L22").value;
    var sperimentali = document.getElementById("Sperimentali").value;
    var lam = document.getElementById("LAM").value;
    var div = 0;
    var compensazione = 0;
    var compensazione2 = 0
    var mark;
    var debug = true;
    if (debug) {
        console.log(os);
        console.log(matematica);
        console.log(oc);
        console.log(arti);
        console.log(fisica);
        console.log(chimica);
        console.log(biologia);
        console.log(italiano);
        console.log(l2);
        console.log(l3);
        console.log(filosofia);
        console.log(geografia);
        console.log(storia);
        console.log(os2);
        console.log(matematica2);
        console.log(italiano2);
        console.log(l22);
        console.log(sperimentali);
        console.log(lam);
    }
    if (checkValidity(l3) &&checkValidity(lam) && checkValidity(os) && checkValidity(os2) && checkValidity(matematica) && checkValidity(matematica2) && checkValidity(oc) && checkValidity(arti) && checkValidity(fisica) && checkValidity(chimica) && checkValidity(biologia) && checkValidity(sperimentali) && checkValidity(italiano) && checkValidity(l2) && checkValidity(l22) && checkValidity(filosofia) && checkValidity(geografia) && checkValidity(storia)) {
        console.log("All value are ok, proceeding...");
        //Averages
        document.getElementById("media_anno").innerHTML = (parseFloat(l3)+parseFloat(oc) + parseFloat(os) + parseFloat(matematica) + parseFloat(arti) + parseFloat(chimica) + parseFloat(fisica) + parseFloat(biologia) + parseFloat(italiano) + parseFloat(l2) + parseFloat(filosofia) + parseFloat(geografia) + parseFloat(storia)) / 12;;
        document.getElementById("media_mat").innerHTML = (parseFloat(os2) + parseFloat(matematica2) + parseFloat(italiano2) + parseFloat(l22) + parseFloat(sperimentali)) / 5;
        //os
        os = (Math.ceil(parseFloat(os) + parseFloat(os2))) / 2;
        div++
        if (os < 4) {
            os = 2 * parseFloat(os);
            compensazione = compensazione + (8 - parseFloat(os));
            div++;
            console.log("OS is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(os) - 4);
        }
        //Math
        matematica = (Math.ceil(parseFloat(matematica) + parseFloat(matematica2))) / 2;;
        div++;
        if (matematica < 4) {
            matematica = 2 * matematica;
            compensazione = compensazione + (8 - parseFloat(matematica));
            div++;
            console.log("Math is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(matematica) - 4);
        }
        //Ita
        italiano = (Math.ceil(parseFloat(italiano) + parseFloat(italiano2))) / 2;
        div++
        if (italiano < 4) {
            italiano = 2 * italiano;
            compensazione = compensazione + (8 - parseFloat(italiano));
            div++;
            console.log("Ita is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(italiano) - 4);
        }
        //l2
        l2 = (Math.ceil(parseFloat(l2) + parseFloat(l22))) / 2;
        div++
        if (l2 < 4) {
            l2 = 2 * l2;
            compensazione = compensazione + (8 - parseFloat(l2));
            div++;
            console.log("l2 is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(l2) - 4);
        }
        //l3
        div++
        if (l3 < 4) {
            l3 = 2 * l3;
            compensazione = compensazione + (8 - parseFloat(l3));
            div++;
            console.log("filosofia is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(l3) - 4);
        }
        //sperimentali
        switch (document.getElementById("scelta_sperimentali").value) {
            case 1:
                filosofia = (Math.ceil((parseFloat(sperimentali) + parseFloat(filosofia)))) / 2;
            case 2:
                geografia = (Math.ceil((parseFloat(sperimentali) + parseFloat(geografia)))) / 2;
            case 3:
                storia = (Math.ceil((parseFloat(sperimentali) + parseFloat(storia)))) / 2;
                break;
        }
        div++
        if (filosofia < 4) {
            filosofia = 2 * filosofia;
            compensazione = compensazione + (8 - parseFloat(filosofia));
            div++;
            console.log("filosofia is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(filosofia) - 4);
        }
        div++
        if (geografia < 4) {
            geografia = 2 * geografia;
            compensazione = compensazione + (8 - parseFloat(geografia));
            div++;
            console.log("geografia is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(geografia) - 4);
        }
        div++
        if (storia < 4) {
            storia = 2 * storia;
            compensazione = compensazione + (8 - parseFloat(storia));
            div++;
            console.log("storia is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(storia) - 4);
        }
        //oc
        div++
        if (oc < 4) {
            oc = 2 * oc;
            compensazione = compensazione + (8 - parseFloat(oc));
            div++;
            console.log("oc is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(oc) - 4);
        }
        //arti
        div++
        if (arti < 4) {
            arti = 2 * arti;
            compensazione = compensazione + (8 - parseFloat(arti));
            div++;
            console.log("arti is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(arti) - 4);
        }
        //chimica
        div++
        if (chimica < 4) {
            chimica = 2 * chimica;
            compensazione = compensazione + (8 - parseFloat(chimica));
            div++;
            console.log("chimica is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(chimica) - 4);
        }
        //fisica
        div++
        if (fisica < 4) {
            fisica = 2 * fisica;
            compensazione = compensazione + (8 - parseFloat(fisica));
            div++;
            console.log("fisica is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(fisica) - 4);
        }
        //biologia
        div++
        if (biologia < 4) {
            biologia = 2 * biologia;
            compensazione = compensazione + (8 - parseFloat(biologia));
            div++;
            console.log("biologia is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(biologia) - 4);
        }
        //LAM
        div++
        if (lam < 4) {
            chimica = 2 * chimica;
            compensazione = compensazione + (8 - parseFloat(lam));
            div++;
            console.log("lam is not sufficent");
        } else {
            compensazione2 = compensazione2 + (parseFloat(lam) - 4);
        }
        if (debug) {
            console.log(os);
            console.log(matematica);
            console.log(oc);
            console.log(arti);
            console.log(fisica);
            console.log(chimica);
            console.log(biologia);
            console.log(italiano);
            console.log(l2);
            console.log(sperimentali);
            console.log(lam);
        }
        var compensazione3 = parseFloat(compensazione2) - parseFloat(compensazione);
        document.getElementById("compensazione").innerHTML = compensazione2 + " - " + compensazione + " = " + compensazione3;
        console.log(div);
        mark = (parseFloat(os) + parseFloat(oc) + parseFloat(matematica) + parseFloat(arti) + parseFloat(chimica) + parseFloat(fisica) + parseFloat(biologia) + parseFloat(italiano) + parseFloat(l2) + parseFloat(lam)) / parseFloat(div);
        document.getElementById("nota_finale").innerHTML = mark;
        if (mark >= 4) {
            document.getElementById("decisione").innerHTML = "Promosso";
        } else {
            document.getElementById("decisione").innerHTML = "Non promosso";
        }
    } else {
        console.log("Fail: some value are not valid")
        document.getElementById("decisione").innerHTML = "ERROR, check console for more informations";
    }
}